import ROOT
from ROOT.URANIE import DataServer, Sampler

ds = DataServer.TDataServer()
att_1 = DataServer.TNormalDistribution("x1", -2., 0.05)
att_2 = DataServer.TNormalDistribution("x2", 4., 0.4)

ds.addAttribute(att_1)
ds.addAttribute(att_2)

sampling = Sampler.TSampling(ds, "lhs", 10000)
sampling.generateSample()

ds.addAttribute("sum", "x1+x2")
ds.addAttribute("div", "x1/x2")

c = ROOT.TCanvas()
c.Divide(2, 2)
for idx, att in enumerate(ds.getListOfAttributes()):
    c.cd(idx+1)
    ds.draw(att.GetName())
