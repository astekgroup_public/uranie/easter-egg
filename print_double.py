import argparse

parser = argparse.ArgumentParser(description='print double')
parser.add_argument('input', type=int)
args = parser.parse_args()
print(2*args.input)

# https://docs.python.org/3/library/argparse.html
