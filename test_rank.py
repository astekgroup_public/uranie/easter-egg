from URANIE import DataServer
import argparse

parser = argparse.ArgumentParser(description='Test Rank')
parser.add_argument('filename', help='input data filename')
args = parser.parse_args()

ds = DataServer.TDataServer()
ds.fileDataRead(args.filename)

ds.computeRank()
ds.scan()
