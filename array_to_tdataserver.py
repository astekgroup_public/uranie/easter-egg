from URANIE import DataServer
import ROOT
import numpy

x = numpy.array([1, 2, 3], dtype=numpy.float64)
print(x)

df = ROOT.RDF.MakeNumpyDataFrame({'x': x})
df.Display().Print()
df.Snapshot('tree', 'tree.root')

ds = DataServer.TDataServer()
ds.ntupleDataRead('tree.root', 'tree')
ds.scan()

# https://root.cern/doc/master/df032__MakeNumpyDataFrame_8py.html
