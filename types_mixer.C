struct MySize {
    size_t value;
};

void mix_types(const vector<string>& words) {
    ROOT::RDataFrame df(words.size());
    auto df_2 = df.Define("word", [&words](unsigned long long rdfentry_){
        return words.at(rdfentry_);
    }, {"rdfentry_"})
    .Define("size", [](const string& word){return word.size();}, {"word"})
    .Define("half", [](size_t size){return size/2.;}, {"size"})
    .Define("coordinates", [](size_t size, double half){
        return vector<double> {static_cast<double>(size), half};
    }, {"size", "half"})
    .Define("my_size", [](size_t size){
        return MySize {size};
    }, {"size"})
    .Define("my_value", [](const MySize& size){
        return size.value;
    }, {"my_size"});
    df_2.Display()->Print();

    for(const auto& name: df_2.GetColumnNames())
        cout  << name << ": " << df_2.GetColumnType(name) << endl;
}
// https://root.cern/doc/v624/group__dataframe.html
// https://en.cppreference.com/w/cpp/container/vector

void types_mixer(){}
