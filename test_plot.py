# pip3 install --user -r requirements.txt
# MPLBACKEND=Qt5Agg python3 test_plot.py

from URANIE import DataServer, Sampler
import ROOT
from matplotlib import pyplot

ds = DataServer.TDataServer()
ds.addAttribute(DataServer.TNormalDistribution('x'))

sampling = Sampler.TSampling(ds)
sampling.generateSample()
ds.scan('', ds.getIteratorName() + '<=10')

df = ROOT.RDataFrame(ds.getTuple())
df.Display().Print()

pyplot.plot(df.AsNumpy()['x'])
pyplot.show()
