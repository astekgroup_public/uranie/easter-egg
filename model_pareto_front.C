void model_pareto_front(const string& image_filename, const string& tree_name,
    const string& tree_filename) {
    ROOT::RDataFrame df(tree_name, tree_filename);
    df.Display()->Print();

    auto objective_1 = df.Take<double>("obj1").GetValue();
    auto objective_2 = df.Take<double>("obj2").GetValue();

    TF1 model("Model", "[0]*exp(-x/[1])");
    model.SetParameters(1., 1.);

    TCanvas canvas;
    TGraph data(objective_1.size(), objective_1.data(), objective_2.data());
    data.SetTitle("Pareto Front");
    data.GetXaxis()->SetTitle("Objective 1");
    data.GetYaxis()->SetTitle("Objective 2");

    data.Fit(&model);
    cout << "Formula: " << model.GetExpFormula() << endl;

    data.Draw("A*");
    canvas.SaveAs(image_filename.data());
}

// https://root.cern/doc/v624/classTGraph.html
// https://root.cern/doc/v624/classTF1.html
// https://root-forum.cern.ch/t/exponential-fit/21260
