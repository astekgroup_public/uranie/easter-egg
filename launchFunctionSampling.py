
#!/usr/bin/env python

from URANIE import DataServer, Sampler, Launcher
import ROOT

def export(suffix):
    # Create a TDataServer
    ds = DataServer.TDataServer("dsFlowrate","ds for flowrate")
    # Add the eight attributes of the study with uniform law
    ds.addAttribute( DataServer.TUniformDistribution("rw", 0.05, 0.15))
    ds.addAttribute( DataServer.TUniformDistribution("r", 100.0, 50000.0))
    ds.addAttribute( DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
    ds.addAttribute( DataServer.TUniformDistribution("tl", 63.1, 116.0))
    ds.addAttribute( DataServer.TUniformDistribution("hu", 990.0, 1110.0))
    ds.addAttribute( DataServer.TUniformDistribution("hl", 700.0, 820.0))
    ds.addAttribute( DataServer.TUniformDistribution("l", 1120.0, 1680.0))
    ds.addAttribute( DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

    # Generate the sampling from the TDataServer
    sampling = Sampler.TSampling(ds, "srs", 500)
    sampling.generateSample()

    # Load the function in the UserFunction macros file
    ROOT.gROOT.LoadMacro("UserFunctions.C")

    lf = Launcher.TLauncherFunction(ds, "flowrateModel","rw:r:tu:tl:hu:hl:l:kw","ymod")
    # Evaluate the function on all the design of experiments
    lf.run()
    ds.getListOfAttributes().ls()
    ds.exportData("flowrateUniformDesign_" + suffix + ".dat")

export("train")
export("test")
