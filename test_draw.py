from URANIE import DataServer, Sampler
import ROOT
import argparse

parser = argparse.ArgumentParser(description='Draw Test')
parser.add_argument('filename', help='PNG image filename')
args = parser.parse_args()

ds = DataServer.TDataServer()
distribution = DataServer.TNormalDistribution('normal')
ds.addAttribute(distribution)

sampler = Sampler.TSampling(ds)
sampler.generateSample()

ds.drawTufte('normal:normal')
ds.draw('normal')
ROOT.gPad.SaveAs(args.filename)
