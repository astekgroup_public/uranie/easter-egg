#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.8.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()

###
### SHAPER component
###

from SketchAPI import *

from salome.shaper import model

model.begin()
partSet = model.moduleDocument()
model.addParameter(partSet, "p0", '-0.42813')
model.addParameter(partSet, "p1", '1.02859')
model.addParameter(partSet, "parameter_1", '0.5')
model.addParameter(partSet, "parameter_2", 'p0 + p1 * parameter_1')

### Create Part
Part_1 = model.addPart(partSet)
Part_1_doc = Part_1.document()

### Create Sketch
Sketch_1 = model.addSketch(Part_1_doc, model.defaultPlane("XOY"))

### Create SketchLine
SketchLine_1 = Sketch_1.addLine(0.25, -0.25, -0.25, -0.25)

### Create SketchProjection
SketchProjection_1 = Sketch_1.addProjection(model.selection("VERTEX", "PartSet/Origin"), False)
SketchPoint_1 = SketchProjection_1.createdFeature()

### Create SketchLine
SketchLine_2 = Sketch_1.addLine(-0.25, -0.25, -0.25, 0.25)

### Create SketchLine
SketchLine_3 = Sketch_1.addLine(-0.25, 0.25, 0.25, 0.25)

### Create SketchLine
SketchLine_4 = Sketch_1.addLine(0.25, 0.25, 0.25, -0.25)
Sketch_1.setCoincident(SketchLine_4.endPoint(), SketchLine_1.startPoint())
Sketch_1.setCoincident(SketchLine_1.endPoint(), SketchLine_2.startPoint())
Sketch_1.setCoincident(SketchLine_2.endPoint(), SketchLine_3.startPoint())
Sketch_1.setCoincident(SketchLine_3.endPoint(), SketchLine_4.startPoint())

### Create SketchLine
SketchLine_5 = Sketch_1.addLine(0.25, -0.25, -0.25, 0.25)
SketchLine_5.setAuxiliary(True)

### Create SketchLine
SketchLine_6 = Sketch_1.addLine(-0.25, -0.25, 0.25, 0.25)
SketchLine_6.setAuxiliary(True)
Sketch_1.setCoincident(SketchLine_1.startPoint(), SketchLine_5.startPoint())
Sketch_1.setCoincident(SketchLine_2.startPoint(), SketchLine_6.startPoint())
Sketch_1.setCoincident(SketchLine_3.startPoint(), SketchLine_5.endPoint())
Sketch_1.setCoincident(SketchLine_4.startPoint(), SketchLine_6.endPoint())
Sketch_1.setCoincident(SketchAPI_Point(SketchPoint_1).coordinates(), SketchLine_5.result())
Sketch_1.setCoincident(SketchAPI_Point(SketchPoint_1).coordinates(), SketchLine_6.result())
Sketch_1.setHorizontal(SketchLine_1.result())
Sketch_1.setVertical(SketchLine_2.result())
Sketch_1.setHorizontal(SketchLine_3.result())
Sketch_1.setVertical(SketchLine_4.result())
Sketch_1.setLength(SketchLine_3.result(), "parameter_1")
Sketch_1.setEqual(SketchLine_3.result(), SketchLine_4.result())

### Create SketchLine
SketchLine_7 = Sketch_1.addLine(0.04308249999999997, -0.04308249999999997, -0.04308249999999997, -0.04308249999999997)

### Create SketchLine
SketchLine_8 = Sketch_1.addLine(-0.04308249999999997, -0.04308249999999997, -0.04308249999999997, 0.04308249999999997)

### Create SketchLine
SketchLine_9 = Sketch_1.addLine(-0.04308249999999997, 0.04308249999999997, 0.04308249999999997, 0.04308249999999997)

### Create SketchLine
SketchLine_10 = Sketch_1.addLine(0.04308249999999997, 0.04308249999999997, 0.04308249999999997, -0.04308249999999997)
Sketch_1.setCoincident(SketchLine_10.endPoint(), SketchLine_7.startPoint())
Sketch_1.setCoincident(SketchLine_7.endPoint(), SketchLine_8.startPoint())
Sketch_1.setCoincident(SketchLine_8.endPoint(), SketchLine_9.startPoint())
Sketch_1.setCoincident(SketchLine_9.endPoint(), SketchLine_10.startPoint())

### Create SketchLine
SketchLine_11 = Sketch_1.addLine(0.04308249999999997, -0.04308249999999997, -0.04308249999999997, 0.04308249999999997)
SketchLine_11.setAuxiliary(True)

### Create SketchLine
SketchLine_12 = Sketch_1.addLine(-0.04308249999999997, -0.04308249999999997, 0.04308249999999997, 0.04308249999999997)
SketchLine_12.setAuxiliary(True)
Sketch_1.setCoincident(SketchLine_7.startPoint(), SketchLine_11.startPoint())
Sketch_1.setCoincident(SketchLine_8.startPoint(), SketchLine_12.startPoint())
Sketch_1.setCoincident(SketchLine_9.startPoint(), SketchLine_11.endPoint())
Sketch_1.setCoincident(SketchLine_10.startPoint(), SketchLine_12.endPoint())
Sketch_1.setCoincident(SketchAPI_Point(SketchPoint_1).coordinates(), SketchLine_11.result())
Sketch_1.setCoincident(SketchAPI_Point(SketchPoint_1).coordinates(), SketchLine_12.result())
Sketch_1.setHorizontal(SketchLine_7.result())
Sketch_1.setVertical(SketchLine_8.result())
Sketch_1.setHorizontal(SketchLine_9.result())
Sketch_1.setVertical(SketchLine_10.result())
Sketch_1.setEqual(SketchLine_9.result(), SketchLine_10.result())
Sketch_1.setLength(SketchLine_9.result(), "parameter_2")
model.do()

### Create Extrusion
Extrusion_1 = model.addExtrusion(Part_1_doc, [model.selection("FACE", "Sketch_1/Face-SketchLine_4r-SketchLine_3r-SketchLine_2r-SketchLine_1r-SketchLine_7f-SketchLine_8f-SketchLine_9f-SketchLine_10f")], model.selection(), 1, 0, "Faces|Wires")

model.end()

###
### SHAPERSTUDY component
###

model.publishToShaperStudy()
import SHAPERSTUDY
Extrusion_1_1, = SHAPERSTUDY.shape(model.featureStringId(Extrusion_1))

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
