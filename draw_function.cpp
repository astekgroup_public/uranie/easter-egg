void draw_function(string filename, double x_min = -10., double x_max = 10.,
    size_t num_steps = 1000) {
    const double delta = (x_max - x_min) / static_cast<double>(num_steps);

    ROOT::RDataFrame empty(num_steps);
    int i_counter = 0;
    auto filled = empty
        .Define("x", [&i_counter, x_min, delta](){
            return x_min + static_cast<double>(i_counter++)*delta;
        })
        .Define("y", [](double x){
            return x*sin(x) + gRandom->Gaus(0.0, 0.1);
        }, {"x"});

    auto graph = filled.Graph("x", "y");
    graph->Draw();
    gPad->SaveAs(filename.data());
}

// References
// https://root.cern/doc/v622/classROOT_1_1RDataFrame.html
// https://root.cern/doc/v622/df001__introduction_8C.html
// https://en.cppreference.com/w/cpp/language/lambda
